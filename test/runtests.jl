using SomeCoolColourMaps
using Colors
using Test

@testset "Maps" begin
    cm = cmap("D1")
    @test cm[1] ≈ RGBA{Float64}(0.12791698116379835,0.31613336222469474,0.8583632941589665,1.0)
end
