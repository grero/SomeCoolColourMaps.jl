module SomeCoolColourMaps
using Interpolations
using Printf
using LinearAlgebra
include("utilities.jl")
include("cmap.jl")

end # module
